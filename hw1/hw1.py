import sys

def read_data():
	data = []
	#with open("./../rainfall.csv", 'r') as f:
	with open("./rainfall.csv", 'r') as f:
		labels = (f.readline())[:-1].split(",")
		for line in f:
			line = line[:-1]
			lineItem = {}
			info = line.split(",")
			for i in range(0, len(labels)):
				if "." in info[i]:
					lineItem[labels[i]] = float(info[i])
				else:
					lineItem[labels[i]] = int(info[i])
			data.append(lineItem)
	#print(data)
	return data

def dates(data, start=None, end=None):
	subSection = []
	if(start == None):
		start = -sys.maxsize - 1
	if(end == None):
		end = sys.maxsize
	for d in data:
		if(d["year"] >= start and d["year"] <= end):
			subSection.append(d)
	return subSection

def paginate(data, offset=0, limit=-1):
	if(limit == -1):
		limit = len(data)
	if(offset > len(data) or offset < 0):
		return None
	subSection = []
	for d in data[offset:]:
		if(len(subSection) >= limit):
			continue
		else:
			subSection.append(d)
	return subSection